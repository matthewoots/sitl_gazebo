/****************************************************************************
 *
 *   Copyright (c) 2021 Temasek Laboratory
 *
 ****************************************************************************/
/**
 * @brief Sysid Plugin
 *
 * This plugin sets state of the uav for sysid
 *
 * @author matthew <mwoo@nus.edu.sg>
 */

#ifndef _GAZEBO_SYSID_PLUGIN_HH_
#define _GAZEBO_SYSID_PLUGIN_HH_

#include <math.h>
#include <cstdio>
#include <cstdlib>
#include <queue>
#include <random>
#include <fstream>
#include <iostream>

#include <sdf/sdf.hh>
#include <common.h>

#include <gazebo/common/Plugin.hh>
#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/physics/physics.hh>
#include <ignition/math.hh>
#include <gazebo/gazebo_client.hh>

#include <sysid_msg.pb.h>
#include <stateCallback.pb.h>
#include "yaml-cpp/yaml.h"

namespace gazebo
{
// Default values
static const std::string kDefaultStateTopic = "~/woodstock/callback";

static const ignition::math::Vector3d kDefaultSetValues = {8.54858e-07, 8.54858e-07, 8.54858e-07};
static const ignition::math::Quaterniond kDefaultSetOrientationValues = {8.54858e-07, 8.54858e-07, 8.54858e-07};

typedef const boost::shared_ptr<const test_suite_msgs::msgs::stateCallback> StatePtr;
typedef const boost::shared_ptr<const gazebo::msgs::PosesStamped> PosePtr;

class GAZEBO_VISIBLE SysidPlugin : public ModelPlugin
{
public:
  SysidPlugin()
      : ModelPlugin(),
        state_sub_topic_{kDefaultStateTopic}
        {}
  virtual ~SysidPlugin();
  virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
  void Update(const common::UpdateInfo &_info);
  void SysidStateCallback(StatePtr& msg);
  void convertMsg();

  /// \brief a pointer to the model this plugin was loaded by
  physics::ModelPtr model;
  /// \brief object for callback connection
  event::ConnectionPtr updateConnection;

protected:

private:
  std::string state_sub_topic_;
  std::string namespace_;
  physics::ModelPtr model_;
  physics::WorldPtr world_;
  event::ConnectionPtr updateConnection_;
  bool _gzSetState;

  ignition::math::Vector3d modelSetPosition;
  ignition::math::Vector3d modelSetVelocity;
  ignition::math::Vector3d modelSetAcceleration;
  ignition::math::Quaterniond modelSetOrientation;
  ignition::math::Vector3d modelSetAngularVelocity;
  ignition::math::Vector3d modelSetAngularAcceleration;

  transport::NodePtr node_handle_;
  transport::SubscriberPtr state_sub_;

  bool atstate;
  bool startsysid;

  transport::PublisherPtr sys_pub_;
  test_suite_msgs::msgs::sysid_msg sys_msg;
  common::Time start_time;
  common::Time logger_time;
  int counter = 0;
  bool ismoved = false;
  bool initialised = false;

  void sysidParamUpdate();


};     // class GAZEBO_VISIBLE GroundtruthPlugin

GZ_REGISTER_MODEL_PLUGIN(SysidPlugin)


SysidPlugin::~SysidPlugin()
{
  if (updateConnection_)
    updateConnection_->~Connection();
  world_->Reset();
}

void SysidPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{
  // Initlise Setstate parameters
  sysidParamUpdate();

  // Create our node for communication
  node_handle_ = transport::NodePtr(new transport::Node());
  node_handle_->Init(namespace_);

  this->model = _model;
  world_ = this->model->GetWorld();
  start_time = world_->SimTime();
  logger_time = world_->SimTime();
  gzmsg << "[SysidPlugin] Start @ " << start_time.Double() << ".\n";
  this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&SysidPlugin::Update, this, std::placeholders::_1));

  getSdfParam<std::string>(_sdf, "stateSubTopic", state_sub_topic_, kDefaultStateTopic);
  getSdfParam<bool>(_sdf, "gzSetState", _gzSetState, false);

  getSdfParam<ignition::math::Vector3d>(_sdf, "ModelSetPosition_", modelSetPosition, kDefaultSetValues);
  getSdfParam<ignition::math::Vector3d>(_sdf, "ModelSetVelocity_", modelSetVelocity, kDefaultSetValues);
  getSdfParam<ignition::math::Vector3d>(_sdf, "ModelSetAcceleration_", modelSetAcceleration, kDefaultSetValues);
  getSdfParam<ignition::math::Quaterniond>(_sdf, "ModelSetOrientation_", modelSetOrientation, kDefaultSetOrientationValues);
  getSdfParam<ignition::math::Vector3d>(_sdf, "ModelSetAngularVelocity_", modelSetAngularVelocity, kDefaultSetValues);
  getSdfParam<ignition::math::Vector3d>(_sdf, "ModelSetAngularAcceleration_", modelSetAngularAcceleration, kDefaultSetValues);

  gzmsg << "[SysidPlugin] ModelSetPosition_: " << modelSetPosition << "\n";
  gzmsg << "[SysidPlugin] ModelSetVelocity_: " << modelSetVelocity << "\n";
  gzmsg << "[SysidPlugin] ModelSetAcceleration_: " << modelSetAcceleration << "\n";
  gzmsg << "[SysidPlugin] modelSetOrientation: " << modelSetOrientation << "\n";
  gzmsg << "[SysidPlugin] ModelSetAngularVelocity_: " << modelSetAngularVelocity << "\n";
  gzmsg << "[SysidPlugin] ModelSetAngularAcceleration_: " << modelSetAngularAcceleration << "\n";

  state_sub_ = node_handle_->Subscribe(state_sub_topic_,&SysidPlugin::SysidStateCallback, this);

  sys_pub_ = node_handle_->Advertise<test_suite_msgs::msgs::sysid_msg>("~/" + this->model->GetName() + "/sysid", 10);
}

void SysidPlugin::Update(const common::UpdateInfo &_info)
{
  common::Time log_time = world_->SimTime();
  // Delay in seconds
  double lapse = 10.0;
  double loggerlapse = 1.0;

  ignition::math::Pose3d T_W_I = this->model->GetLink("base_link")->WorldPose();
  ignition::math::Quaterniond C_W_I = T_W_I.Rot();
  // gzmsg << "[SysidPlugin] Rot: " << C_W_I.Euler() << "\n";

  ignition::math::Vector2d worldVel = ignition::math::Vector2d(modelSetVelocity.X() * cos(C_W_I.Euler().Z()),modelSetVelocity.X() * sin(C_W_I.Euler().Z()));
  ignition::math::Vector3d RelativeVelocity = ignition::math::Vector3d(worldVel.X(), worldVel.Y(), modelSetVelocity.Z());
  // gzmsg << "[SysidPlugin] RelativeVelocity: " << RelativeVelocity << "\n";

  // Controller from sysid messages to make adjustments until the UAV is in SetModel condition!

  ignition::math::Vector3d force = this->model->GetLink("base_link")->RelativeForce();
  ignition::math::Vector3d moment = this->model->GetLink("base_link")->RelativeTorque();
  // gzerr << "F(" << force << ") M(" << moment <<")\n";
  // if (log_time.Double() - start_time.Double() >= lapse)
  if (!startsysid) initialised = false;
  else
  {
    // gzmsg << "[SysidPlugin] Sending State ... " << "\n";
    // gzmsg << "[SysidPlugin] Time: " << log_time << "\n";
    // gzmsg << "[SysidPlugin] TimeDiff: " << log_time.Double() - start_time.Double() << "\n";

    if (!initialised && _gzSetState){
      // We want to keep the yaw the same pi/8 = 45, pi/4 = 90 rotation is positive clockwise X is Pitch Y = Roll
      ignition::math::Pose3d NPose = T_W_I;
      NPose.Rot().X() = modelSetOrientation.X();
      NPose.Rot().Y() = modelSetOrientation.Y();
      this->model->SetWorldPose(NPose);
      gzmsg << "[SysidPlugin] Rot(): " << NPose.Rot() << "\n";

      this->model->GetLink("base_link")->SetForce(modelSetAcceleration);
      // When the rotational inertia of an object is constant, the angular acceleration is proportional to torque.
      this->model->GetLink("base_link")->SetTorque(modelSetAngularAcceleration);
      // this->model->SetLinearVel(RelativeVelocity);
      gzmsg << "[SysidPlugin] Vel(): " << this->model->WorldLinearVel() << "\n";
      this->model->SetAngularVel(modelSetAngularVelocity);
      initialised = true;
    }

    // start_time = log_time;
    // logger_time = log_time;
    // ismoved = true;
  }

  // publish sysid msg at full sim rate
  convertMsg();
  sys_pub_->Publish(sys_msg);
  counter++;
  // if (!startsysid) return;
  // gzmsg << "[SysidPlugin] started "<< startsysid <<"\n";
}

void SysidPlugin::sysidParamUpdate() {

  std::ifstream inFile;
  inFile.open("sysid_include/param/trim1.yaml");
  // YAML::Parser parser(inFile);
  return;
}

void SysidPlugin::convertMsg()
{
  // fill sysid msg
  gazebo::msgs::Vector3d* linear_velocity = new gazebo::msgs::Vector3d();
    linear_velocity->set_x(modelSetVelocity.X());
    linear_velocity->set_y(modelSetVelocity.Y());
    linear_velocity->set_z(modelSetVelocity.Z());
    sys_msg.set_allocated_linear_velocity(linear_velocity);
  gazebo::msgs::Vector3d* linear_acceleration = new gazebo::msgs::Vector3d();
    linear_acceleration->set_x(modelSetAcceleration.X());
    linear_acceleration->set_y(modelSetAcceleration.Y());
    linear_acceleration->set_z(modelSetAcceleration.Z());
    sys_msg.set_allocated_linear_acceleration(linear_acceleration);
  gazebo::msgs::Vector3d* angular_velocity = new gazebo::msgs::Vector3d();
    angular_velocity->set_x(modelSetAngularVelocity.X());
    angular_velocity->set_y(modelSetAngularVelocity.Y());
    angular_velocity->set_z(modelSetAngularVelocity.Z());
    sys_msg.set_allocated_angular_velocity(angular_velocity);
  gazebo::msgs::Vector3d* angular_acceleration = new gazebo::msgs::Vector3d();
    angular_acceleration->set_x(modelSetAngularAcceleration.X());
    angular_acceleration->set_y(modelSetAngularAcceleration.Y());
    angular_acceleration->set_z(modelSetAngularAcceleration.Z());
    sys_msg.set_allocated_angular_acceleration(angular_acceleration);
  gazebo::msgs::Quaternion* orientation = new gazebo::msgs::Quaternion();
    orientation->set_w(modelSetOrientation.W());
    orientation->set_x(modelSetOrientation.X());
    orientation->set_y(modelSetOrientation.Y());
    orientation->set_z(modelSetOrientation.Z());
    sys_msg.set_allocated_orientation(orientation);
  sys_msg.set_time((world_->SimTime()).Double());
  // gzmsg << "[SysidPlugin] Orientation: " << modelSetOrientation.X() << " " << modelSetOrientation.Y() << " " << modelSetOrientation.Z() << " " << modelSetOrientation.W() << ".\n";
}


void SysidPlugin::SysidStateCallback(StatePtr& msg)
{
  startsysid = msg->start();
  atstate = msg->atstate();
}

} // namespace gazebo
#endif // _GAZEBO_SYSID_PLUGIN_HH_
