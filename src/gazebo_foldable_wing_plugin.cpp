/****************************************************************************
 *
 *   Copyright (c) 2021 Temasek Laboratory
 *
 ****************************************************************************/
/**
 * @brief Foldable Wing Plugin
 *
 * This plugin for Foldable Wing
 *
 * @author matthew <mwoo@nus.edu.sg>
 */

#ifndef _GAZEBO_FOLDABLE_WING_PLUGIN_HH_
#define _GAZEBO_FOLDABLE_WING_PLUGIN_HH_

#include <math.h>
#include <cstdio>
#include <cstdlib>
#include <queue>
#include <random>
#include <fstream>
#include <iostream>

#include <sdf/sdf.hh>
#include <common.h>

#include <gazebo/common/Plugin.hh>
#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/physics/physics.hh>
#include <ignition/math.hh>
#include <gazebo/gazebo_client.hh>

#include <sysid_msg.pb.h>
#include <stateCallback.pb.h>
#include "yaml-cpp/yaml.h"

namespace gazebo
{
// Default values
static const std::string kDefaultTopic = "default";

static const float kDefaultSetValues = 0;

class GAZEBO_VISIBLE FoldableWingPlugin : public ModelPlugin
{
public:
  FoldableWingPlugin()
      : ModelPlugin()
        {}
  virtual ~FoldableWingPlugin();
  virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
  void Update(const common::UpdateInfo &_info);

  /// \brief a pointer to the model this plugin was loaded by
  physics::ModelPtr model;
  /// \brief object for callback connection
  event::ConnectionPtr updateConnection;

protected:

private:
  std::string namespace_;
  physics::ModelPtr model_;
  physics::WorldPtr world_;
  event::ConnectionPtr updateConnection_;

  transport::NodePtr node_handle_;
  std::string parent;
  std::string child;

  std::vector<physics::JointPtr> joint_parent;
  std::vector<physics::JointPtr> joint_child;


};     // class GAZEBO_VISIBLE GroundtruthPlugin

GZ_REGISTER_MODEL_PLUGIN(FoldableWingPlugin)


FoldableWingPlugin::~FoldableWingPlugin()
{
  if (updateConnection_)
    updateConnection_->~Connection();
  world_->Reset();
}

void FoldableWingPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{
  // Create our node for communication
  node_handle_ = transport::NodePtr(new transport::Node());
  node_handle_->Init(namespace_);
  this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&FoldableWingPlugin::Update, this, std::placeholders::_1));

  this->model = _model;
  world_ = this->model->GetWorld();

  getSdfParam<std::string>(_sdf, "jparent", parent, kDefaultTopic);
  getSdfParam<std::string>(_sdf, "jchild", child, kDefaultTopic);

  gzmsg << "[FoldableWingPlugin] Init" << "\n";
}

void FoldableWingPlugin::Update(const common::UpdateInfo &_info)
{
  // gzmsg << "[FoldableWingPlugin] Update" << "\n";
  float parent_angle = this->model->GetJoint(parent)->Position();
  // gzmsg << parent_angle << "\n";
  // We mimic angle of end_foldable_X_joint angle with base_foldable_X_joint
  this->model->GetJoint(child)->SetPosition(0, parent_angle);

}

} // namespace gazebo
#endif // _GAZEBO_FOLDABLE_WING_PLUGIN_HH_
